
public class Run {

	public static void main(String[] args) {
		int a = 4;
		{
			int b = 5;
			a = 6;
			b = 7;
		}
		a = 8;
		//b=1;   b n'existe plus en dehors du bloc form� par les deux acolades o� la variable a �t� d�clar�e
	}

}
