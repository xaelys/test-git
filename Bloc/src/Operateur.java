
public class Operateur {

	public static void main(String[] args) {
		


		//Operateurs num�riques
		int a = 3;
		int b = 5;
		float c = 5.0f;
		System.out.println("---------- Operateurs numeriques ----------");
		System.out.println(+a+" + "+b+ "   = "+ (a+b));	//Addition
		System.out.println(+a+" - "+b+ "   = "+ (a-b));	//Soustraction
		System.out.println(+a+" * "+b+ "   = "+ (a*b));	//Multiplication
		System.out.println(+a+" / "+b+ "   = "+ (a/b));	//Division enti�re si division d'entiers ou d�cimale si une des deux op�randes est d�cimal
		System.out.println(+a+" / "+c+ " = "+ (a/c));	//Division enti�re si division d'entiers ou d�cimale si une des deux op�randes est d�cimal
		System.out.println(+a+" % "+b+ "   = "+ (a%b));	//Reste de la division enti�re
		
		int tmp = a*b+b-a*b;
		int tmp1 = a*b+(b-a)*b;
		System.out.println("test de priorit�: tmp = "+ tmp);
		System.out.println("test de priorit�: tmp1 = "+ tmp1);
		System.out.println();
		
		//Op�rateurs binaires
		//tester avec la calculatrice windows Decimal/hexa
		int bin  = 0x010101;
		int bin2 = 0x101011;
		System.out.println("---------- Operateurs binaires ----------");
		
		System.out.print(+bin+" & "+bin2+ "   = "+ (bin&bin2));	//Et. bit � bit
		if((bin & bin2) == 0x000001) System.out.println("       ou  0x010101 & 0x101011 = 0x000001");
		
		System.out.print(+bin+" | "+bin2+ "  = "+ (bin|bin2));	//Ou. bit � bit
		if((bin | bin2) == 0x111111) System.out.println("  ou  0x010101 | 0x101011 = 0x111111");
		
		System.out.print(+bin+" ^ "+bin2+ "  = "+ (bin^bin2));	//Xor. bit � bit
		if((bin ^ bin2) == 0x111110) System.out.println("  ou  0x010101 ^ 0x101011 = 0x111110");
		
		//System.out.print(+bin+" ~ "+bin2+ "  = "+ (bin|bin2));	//Ou. bit � bit
		//if((~ )) System.out.println("  ou 0x010101 | 0x101011 = 0x101010");
		
		System.out.println(+bin+" >> 2  = "+ (bin>>2));	//D�calage vers la droite
		if((bin >> 2 ) == 0x000101) System.out.println("  ou  0x010101 >> 2 = 0x000101");
		
		System.out.println(+bin2+" << 2  = "+ (bin2<<2));	//D�calage vers la gauche
		if((bin2 << 2 ) == 0x101100) System.out.println("  ou  0x010101 << 2 = 0x101100");
		System.out.println();
		
		//Op�rateurs bool�ens
		System.out.println("---------- Operateurs booleens ----------");
		
		// == Egalit�
		if(a == b) System.out.println("a == b : True");
		else System.out.println("a == b : False");
		
		// != Difference
		if(a != b) System.out.println("a != b : True");
		else System.out.println("a != b : False");
		
		// < Inferiorit� Stricte
		if(a < b) System.out.println("a < b : True");
		else System.out.println("a < b : False");
		
		// <= Inferiorit� ou egale
		if(a < b) System.out.println("a <= b : True");
		else System.out.println("a <= b : False");
		
		// > Superiorit� stricte
		if(a > b) System.out.println("a > b : True");
		else System.out.println("a > b : False");
		
		// >= Superiorit� ou egale
		if(a >= b) System.out.println("a >= b : True");
		else System.out.println("a >= b : False");
		
		// && Et
		if(a != b && a <b) System.out.println("a != b && a <b : True");
		else System.out.println("a != b et a <b : False");
		
		// || Ou
		if(a != b || a >b) System.out.println("a != b || a >b : True");
		else System.out.println("a != b || a >b : False");
		
		// ! Not 
		boolean myBool = false;
		if(!(myBool))System.out.println("It's True");
		else System.out.println("It's False");
		System.out.println();
		
		//Operateurs sur chaine de caracteres
		String str = "toto" + "tata";
		System.out.println("---------- Operateurs sur chaine de caracteres ----------");
		if(str.equals("tototata")) System.out.println("chaine concatenee str = "+str);		//Different de if(("toto" + "tata") == "tototata"))
		str = str +" "+1+" bonjour";	//concatenation d'un entier et d'une string
		System.out.println(str);
		System.out.println();
		
		//Operateurs Affectation
		System.out.println("---------- Operateurs affectations ----------");
		int e = 45;		// =
		System.out.println("e = "+ e);
		e+=10;			// +=   :  e = e + 10
		System.out.println(" += 10 = "+ e);
		e-=10;			// -=   :  e = e - 10
		System.out.println(" -= 10 = "+e);
		e*=2;			// *=   :  e = e * 10
		System.out.println(" *= 10 = "+e);
		e/=2;			// /=   :  e = e / 10
		System.out.println(" /= 2 = "+e);
		e%=2;			// %=   :  e = e % 10
		System.out.println(" %= 2 = "+e);
		e&=2;			// &=   :  e = e & 10
		System.out.println(" &= 10 = "+e);
		e|=2;			// |=   :  e = e | 10
		System.out.println(" |= 10 = "+e);
		e^=2;			// ^=   :  e = e ^ 10
		System.out.println(" ^= 10 = "+e);
		e<<=2;			// <<=  :  e = e << 10
		System.out.println("<<= 10 = "+e);
		e>>=2;			// >>=  :  e = e >> 10
		System.out.println(">>= 10 = "+e);
		System.out.println();
		
		//Operateurs incrementation
		System.out.println("---------- Operateur incrementation ----------");
		e = 10;
		
		System.out.println("e++ = "+ e++);		// e++   :  e = e + 1     e = 10
		System.out.println("++e = "+ ++e);		// ++e					  e = 12
		
		//e--;			// ++   :  e = e - 1
		System.out.println("e-- = "+ e--);
		System.out.println("e-- = "+ --e);
		
		//Operateurs ternaire
		System.out.println("---------- Operateur ternaire ----------");
		//syntaxe :  variable = (expression test) ? valeur affectee si vrai : valeur affectee si faux;
		String result = a<b ? "a plus petit que b" : "b plus petit que a";
		System.out.println(result);
		
	}

}
