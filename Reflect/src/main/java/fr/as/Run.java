package fr.as;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

// master branch 
public class Run {

	public static void main(String[] args) {
		Class<?> metaData = Article.class;
		Method[] methods = metaData.getDeclaredMethods();
		for (Method method : methods) {
			System.out.println("Method " + method.getName());
		}

		System.out.println("----------------------------------------");

		Field[] fields = metaData.getDeclaredFields();
		for (Field field : fields) {
			System.out.println("Field " + field.getName());
		}

		Article article = new Article();
		Class[] paramTypes = null;
		if (args != null) {
			paramTypes = new Class[args.length];
			for (int i = 0; i < args.length; ++i) {
				paramTypes[i] = args[i].getClass();
			}
		}

		try {
			Method m = article.getClass().getMethod("afficher", paramTypes);
			m.invoke(article, args);

			// Article article = new Article();
			java.lang.reflect.Field f;
			f = article.getClass().getField("brand");
			f.set(article, "BIC");
			System.out.println(article);
		} catch (Exception e) {
			System.err.println(e);
		}

	}
}
