package fr.as;

public final class Article {

	private int zeBarCode = 1;

	private String brand = "WATER";

	private double price = 1.0;

	public int getBarCode() {
		return zeBarCode;
	}

	public void setBarCode(int barCode) {
		this.zeBarCode = barCode;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand.toUpperCase();
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		if (price < 0)
			throw new RuntimeException("Bad negative price");
		this.price = price;
	}

	public int getStephan() {
		return 0;
	}

	public void setStephan(int i) {
	}

	private void afficher() {
		System.out.println("m�thode affichage priv�e ! ");
		System.out.println("Article " + this.getBrand());
	}
}
