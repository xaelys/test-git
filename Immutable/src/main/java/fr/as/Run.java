package fr.as;

import java.util.Date;

public class Run {

	public static void main(String[] args) {
		Personne p = new Personne("nom","prenom",new Date());
		System.out.println(p);

		//Personne est il réellement immuable ?
		//region réponse ?
		Date d = p.getDateNaiss();
		d.setYear(d.getYear() + 10);
		System.out.println(p);
		//endregion
	}
}
