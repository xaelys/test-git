package fr.as;

import java.util.Date;

public final class Personne {

	private final String nom;

	private final String prenom;

	private final Date dateNaiss;

	public Personne(String nom, String prenom, Date dateNaiss) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.dateNaiss = dateNaiss;
	}

	public String getNom() {
		return nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public Date getDateNaiss() {
		//return dateNaiss;
		// region ???
		 return new Date(dateNaiss.getTime()); // immuable !
		// endregion
	}

	@Override
	public String toString() {
		StringBuilder result = new StringBuilder("nom=");
		result.append(nom);
		result.append(", prenom=");
		result.append(prenom);
		result.append(", dateNaiss=");
		result.append(dateNaiss);
		return result.toString();
	}
}