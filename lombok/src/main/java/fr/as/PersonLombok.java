import lombok.*;

@ToString
@EqualsAndHashCode
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
@Builder
public class PersonLombok {
    @NonNull
    private final String lastName;
    @NonNull
    private final String firstName;
    @NonNull
    private final Integer age;

    public static void main(String[] args) {
        PersonLombok personLombok = PersonLombok.builder().age(20).firstName("stephan").lastName("pascal").build();
        System.out.println(personLombok);
    }
}