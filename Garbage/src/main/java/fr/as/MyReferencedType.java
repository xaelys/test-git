package fr.as;
//master seconde modification

public class MyReferencedType {

	private String text;

	
	public MyReferencedType( String text ) {
		this.text = text;
	}
	
	public String getText() {
		return text;
	}

	public void finalize() {
		System.out.println("========== Reference released " + text);
		System.out.flush();
	}

}
