package fr.as;

// modification master

// lancer une Jconsole sur ce process
public class TestGC {
	public TestGC(){
		System.out.println("Objet construit");
	}
	
	protected void finalize(){
		System.out.println("############    Objet detruit");
	}
	
	public static void main(String[] args) {
		while (true){
			new TestGC();
		}
	}

}
