package fr.as;

import java.lang.ref.*;
import java.util.*;

public class AllReferences {

	
	@SuppressWarnings( {"unchecked"} )
	public static void main(String [] args) {
		ReferenceQueue<MyReferencedType> queue = new ReferenceQueue<MyReferencedType>(); 

		//S'il ne reste que des soft references sur un objet, celui-ci n'est collect� que si la JVM a besoin de m�moire
		SoftReference sr = new SoftReference<MyReferencedType>( new MyReferencedType( "SR" ) );
		
		//S'il ne reste que des weak references sur un objet, celui-ci est collect�.
		WeakReference wr = new WeakReference<MyReferencedType>( new MyReferencedType( "WR" ) );
		
		/*Cas des phantom references est particulier car il concerne des objets qui sont d�j�
		collect�s. Ce genre de r�f�rence est utilis� afin de conna�tre l'�tat de la m�moire suite aux
		actions du ramasse miette sans interf�rer avec celui-ci*/
		PhantomReference pr = new PhantomReference<MyReferencedType>( new MyReferencedType( "PR" ), queue );

		//S'il reste ne serait ce qu'une strong reference sur un objet, celui-ci n'est pas collect�.
		MyReferencedType ref = new MyReferencedType( "STRONG");

		Vector<Object> v = new Vector<Object>();
		for(int i = 0;  i<2335598; i++) {
			// a environ 9000 le phantom ref est garbag�
			// a environ 90000 le weak ref est garbag�
			// a environ 2335598 le soft ref est garbag�

			 v.addElement( new Date() );
		}

		System.out.println("---Fin du test--------------------------");
		System.out.println("SoftReference == " + sr.get());
		System.out.println("WeakReference == " + wr.get());	
		System.out.println("PhantomReference == " + pr.get());
		System.out.println("La Reference == " + ref);
	}

}

