package fr.as;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Date;

public class Backup {

	public static void backup() throws FileNotFoundException, IOException {
		ArrayList<Object> data = new ArrayList<Object>();
		data.add(10);
		data.add("Hello World !");
		data.add(new Date());
		data.add(new Personne("PASCAL","Stephan"));

		System.out.println("Sauvegarde de :");
		for (Object obj : data)
			System.out.println(obj);

		ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("data.bin"));

		oos.writeObject(data);

		oos.close();

	}

	@SuppressWarnings("unchecked")
	public static void restore() throws FileNotFoundException, IOException, ClassNotFoundException {
		ObjectInputStream ois = new ObjectInputStream(new FileInputStream("data.bin"));
		ArrayList<Object> data = (ArrayList<Object>) ois.readObject();

		ois.close();

		System.out.println("Restore de :");
		for (Object obj : data)
			System.out.println(obj);

	}

	public static void main(String[] args) throws FileNotFoundException, IOException, ClassNotFoundException {
		backup();
		restore();
	}

}
