package fr.as;

import java.io.Serializable;

/*
 * Classe personne qui impl�mente l'interface Serializable
 */
public class Personne implements Serializable {

	private static final long serialVersionUID = 1L;

	private String nom;

	private transient String prenom;

	public Personne(String n, String p) {
		nom = n;
		prenom = p;
	}

	public String toString() {
		return prenom + " " + nom;
	}

}
